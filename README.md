# Documento Oficial del Proyecto
El proyecto global, con todos los apartados de interés, y con gran nivel de detalle se puede encontrar en el siguiendo documento:

[Proyecto de automatización del control de calidad de defectos en bobinas de acero galvanizadas - DESCARGAR PDF](/ComputerVision_QualityControl.pdf)

## Autor y Organismo
- Luis Eduardo Arias Gonzales - 16020
- Universidad Politécnica de Madrid - Escuela Técnica Superior de Ingenieros Industriales (ETSII)

## Descripción general
El objetivo del proyecto radica en un cambio en el proceso actual del control de defectos superficiales presentes en las bobinas de acero galvanizadas que se utilizan para la elaboración de las carrocerías en los automóviles. El proceso actual de inspección en este tipo de requisitos se basa en la clasificación visual de los mapas de saliencia por parte
de operarios experimentados y cualificados. Estos operarios son los encargados de decidir si una bobina de acero debe pasar o no el proceso de inspección de la calidad según sus defectos, todo ello en función de su juicio a la hora de visualizar la imagen.

![Proceso actual del control de calidad en defectos en las bobinas de acero](/imagenes/Esquema_General_Operario.png)

Con esta información, se presenta un cambio radical en este control de calidad, y se presenta como objetivo principal la automatización de este proceso de inspección, mediante técnicas de Visión Artificial de última generación.

![Proceso ideal del control de calidad en defectos en las bobinas de acero](/imagenes/Esquema_General_VA.png)

El escenario de partida está formado por imágenes clasificadas por los operarios en tres categorías: mapas de saliencia sin defectos (asociados a las bobinas que pasarían el control de calidad), mapas de saliencia dudosos (asociados a las bobinas de las que no se tiene certeza de si han de pasar o no el proceso de inspección) y mapas de saliencia con defectos (asociados a las bobinas que, bajo ningún concepto, han de ser utilizadas para la carrocería de los vehículos). La cantidad de imágenes de las que se disponible, por clase, es la siguiente: 
- [ ] 643 mapas de saliencia sin defectos
- [ ] 3762 mapas de saliencia dudosos 
- [ ] 256 mapas de saliencia con defectos

Ante la imposibilidad de realizar cualquier tipo de clasificación verosímil y de garantía mediante técnicas de Inteligencia Artificial con este conjunto de datos inicial, se pretende focalizar el proyecto en dos tareas claras y definidas, las cuales, a su vez, presentan subtareas que aparecen detalladas en cada una de las carpetas: 
1. Obtener un conjunto de datos balanceado y de muestras suficientes con el que se pueda realizar una posterior clasificación de manera coherente. 

    1.1. Aumento de datos mediante operaciones pixelares: [/computer_vision_automation_project/pixelDA](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/pixelDA?ref_type=heads)

    1.2. Aumento de datos mediante respositorios especificos, en concreto, AutoAugment, FastAugment y AdvChain: [/computer_vision_automation_project/augmentation_processes](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes?ref_type=heads)

    1.3. Decisión de la técnica de Data Augmentation más adecuada al conjunto de datos del proyecto: [/computer_vision_automation_project/evaluation_methods](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/evaluation_methods?ref_type=heads)

2. Posteriormente, diseñar un clasificador de última generación capaz de realizar la tarea final de discernir entre aquellas bobinas de acero que deben pasar o no el control de calidad pertinente.

    2.1. Entrenamiento, test y predicción de un clasificador YOLOv8 con el conjunto de datos construido específicamente para este propósito: [/computer_vision_automation_project/yolov8_classification](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/yolov8_classification?ref_type=heads)

## Comentarios de funcionamiento
Antes de nada, con el objetivo de trabajar con el proyecto y las librerías adecuadas, se deben ejecutar los dos siguientes comandos:
```
git clone https://gitlab.com/luisarias2/computer_vision_automation_project.git
```
```
pip install -r requirements.txt
```

Ahora bien, los pasos a seguir para ejecutar todos los apartados que aparecen en la Metodología y Resultados del documento (PDF a descargar), son los siguientes:

1. En primer lugar, se parte de un conjunto de datos en crudo ([/computer_vision_automation_project/global_dataset](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/global_dataset?ref_type=heads)). Estas imágenes de los mapas de saliencia se dividen en sus tres clasificaciones posibles. Para ello, consultar el README de la carpeta [/computer_vision_automation_project/data_operations](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/data_operations?ref_type=heads)

2. El primer aumento de datos que se realiza consiste en operaciones pixelares. Para ello, consultar el README de la carpeta [/computer_vision_automation_project/pixelDA](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/pixelDA?ref_type=heads)

3. El siguiente aumento de datos parte del conjunto de datos generado tras estas operaciones pixelares. Este se basa en técnicas realizadas en repositorios específicos: AutoAugment, FastAugment y AdvChain. Para ello, consultar el README de la carpeta [/computer_vision_automation_project/augmentation_processes](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes?ref_type=heads)

4. Selección de la técnica de aumento de datos procedente de repositorios más adecuada en el proyecto. Esta técnica será la que se utilice para la elaboración de un conjunto de datos nuevo para la clasificación binaria final. Para ello, consultar el README de la carpeta [/computer_vision_automation_project/evaluation_methods](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/evaluation_methods?ref_type=heads)

5. Entrenamiento, test y predicción final en un entorno operativo mediante un clasificador YOLOv8. Para ello, consultar el README de la carpeta [/computer_vision_automation_project/yolov8_classification](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/yolov8_classification?ref_type=heads)

