## DESCRIPCIÓN GENERAL DEL SUBSISTEMA
Esta carpeta se utiliza para realizar una división automática del dataset que se encuentra en la carpeta con la siguiente ruta: [/computer_vision_automation_project/global_dataset](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/global_dataset?ref_type=heads)

Esta división se hace en función de las etiquetas que aparecen en cada uno de los títulos de las imágenes. Ejemplos:
- [ ] 200697119-0.png 

Label 0: Imagen de un mapa de saliencia sin defectos (bobina que no debe usarse en chapas de automóviles)
- [ ] 200694591-1.png 

Label 1: Imagen de un mapa de saliencia dudoso (bobina con dudas de si pasar o no el control de calidad)
- [ ] 200694054-2.png 

Label 2: Imagen de un mapa de saliencia con defectos (bobina que pasa el proceso de inspección)

De esta manera, se generan las tres siguientes carpetas:
- MS sin defectos: 643 imágenes [/computer_vision_automation_project/data_operations/non-defect_images](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/data_operations/non-defect_images?ref_type=heads)
- MS dudosos: 3762 imágenes [/computer_vision_automation_project/data_operations/non-defect_images](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/data_operations/doubt_images?ref_type=heads)
- MS sin defectos: 256 imágenes [/computer_vision_automation_project/data_operations/non-defect_images](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/data_operations/defect_images?ref_type=heads)

## COMENTARIOS DE FUNCIONAMIENTO
Para que se dé un funcionamiento correcto, es necesario seguir los siguientes comandos:
```
cd /computer_vision_automation_project/data_operations
```
```
python3 divide_dataset.py
```
