import os
import shutil

### 1. ALL DATA - Total number of images
folder_path = '/home/luisarias/generator_project/IMGS/coils'
images_list = os.listdir(folder_path)
num_images = len(images_list)
print (f"Hay un total de {num_images} imagenes")

### 2. DIVIDE DATA INTO EACH CORRESPONDING LABEL AND FOLDER
defect_folder_name = "defect_images"
non_defect_folder_name = "non-defect_images"
doubt_folder_name = "doubt_images"
defect_folder_path = os.path.join(os.getcwd(), defect_folder_name)
if not os.path.isdir(defect_folder_path):
    os.makedirs(defect_folder_path)
non_defect_folder_path = os.path.join(os.getcwd(), non_defect_folder_name)
if not os.path.isdir(non_defect_folder_path):
    os.makedirs(non_defect_folder_path)
doubt_folder_path = os.path.join(os.getcwd(), doubt_folder_name)
if not os.path.isdir(doubt_folder_path):
    os.makedirs(doubt_folder_path)

for image_path in images_list:
    name = image_path.split('.')[0]
    # No defect image
    if name[-1] == '0':
        shutil.copy(os.path.join(folder_path, image_path), os.path.join(non_defect_folder_path, image_path))
    # Doubt image
    elif name[-1] == '1':
        shutil.copy(os.path.join(folder_path, image_path), os.path.join(doubt_folder_path, image_path))
    # Defect image
    else:
        shutil.copy(os.path.join(folder_path, image_path), os.path.join(defect_folder_path, image_path))

### 3. OBTAIN THE NUMBER OF IMAGES OF EACH CLASS
defect_images_list = os.listdir(defect_folder_path)
doubt_images_list = os.listdir(doubt_folder_path)
non_defect_images_list = os.listdir(non_defect_folder_path)
print (f"Hay un total de {len(non_defect_images_list)} imagenes sin defectos")
print (f"Hay un total de {len(doubt_images_list)} imagenes dudosas")
print (f"Hay un total de {len(defect_images_list)} imagenes con defectos")