# Preprocesamiento
import tensorflow as tf
import os, sys
import cv2
import numpy as np
import gc, shutil
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
# Clasificador
from tqdm import tqdm
from tensorflow import keras 
from tensorflow.keras.layers import Input,Dense,Flatten,Dropout,Reshape,Concatenate,Conv2D,MaxPooling2D,UpSampling2D,Conv2DTranspose
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.models import Model,Sequential, load_model
from tensorflow.keras.callbacks import ModelCheckpoint, History
from tensorflow.keras.optimizers import Adadelta, RMSprop,SGD,Adam
from tensorflow.keras import regularizers,optimizers
from tensorflow.keras import backend as K
# Resultados
from loguru import logger

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

# PREPROCESAMIENTO
def extraer_datos(folder_path):
    '''
    Función que sirve para crear un array con la siguiente forma:
    (num_imagenes, pixeles_alto-128, pixeles_ancho-256, num_channels-1)
    Para ello, las operaciones intermedias son una conversión a escala de grises y un resize
    '''
    datax = []
    limgs = [s for s in os.listdir(folder_path) if s.endswith('.png')]
    for img in limgs:
        img_array = cv2.imread(os.path.join(folder_path,img))  # convert to array
        img_gris = cv2.cvtColor(img_array, cv2.COLOR_BGR2GRAY)
        img_gris = cv2.resize(img_gris, (128, 256))
        datax.append(img_gris)
    datosimg = np.array(datax).reshape(-1, 128, 256, 1)
    return datosimg

def classes_data(nondefects_folder, doubt_folder, defects_folder):
    datos_OK      = extraer_datos(nondefects_folder)
    datos_insp_OK = extraer_datos(doubt_folder) 
    datos_NOK     = extraer_datos(defects_folder)
    return datos_OK, datos_insp_OK, datos_NOK

def normalization(datos_OK, datos_insp_OK, datos_NOK):
    datos_OK = datos_OK.astype('float32')
    datos_insp_OK = datos_insp_OK.astype('float32')
    datos_NOK = datos_NOK.astype('float32')
    datos_norm_OK=datos_OK/np.max(datos_OK)
    datos_norm_insp_OK = datos_insp_OK/np.max(datos_insp_OK)
    datos_norm_NOK = datos_NOK/ np.max(datos_NOK)
    return datos_norm_OK, datos_norm_insp_OK, datos_norm_NOK

def train_val_test(dataset, train_percent, val_percent, test_percent):
    train_set, val_test_set = train_test_split(dataset, test_size=1-train_percent)
    val_set, test_set = train_test_split(val_test_set, test_size=test_percent/(val_percent+test_percent))
    return train_set, val_set, test_set

def train_val_data(train_OK, train_insp_OK, train_NOK, val_OK, val_insp_OK, val_NOK, test_NOK):
    train_data = np.concatenate((train_OK, train_insp_OK, train_NOK),axis=0)
    train_labels=np.concatenate((train_OK.shape[0]*[0], train_insp_OK.shape[0]*[0],train_NOK.shape[0]*[1]),axis=0)
    # val_data = np.concatenate((val_OK, val_insp_OK, test_NOK),axis=0)
    # val_labels = np.concatenate((val_OK.shape[0]*[0], val_insp_OK.shape[0]*[0],val_NOK.shape[0]*[1]),axis=0)
    val_data = np.concatenate((val_OK, val_insp_OK, val_NOK),axis=0)
    val_labels = np.concatenate((val_OK.shape[0]*[0], val_insp_OK.shape[0]*[0],val_NOK.shape[0]*[1]),axis=0)
    print (train_data.shape , val_data.shape)

    train_labels_one_hot = to_categorical(train_labels)
    val_labels_one_hot = to_categorical(val_labels)
    # Display the change for category label using one-hot encoding
    print('Original label:', train_labels[500])
    print('After conversion to one-hot:', train_labels_one_hot[500])
    return train_data, val_data, train_labels_one_hot, val_labels_one_hot

def classifier(input_img):
    conv1 = Conv2D(32, (5, 5), activation='relu', padding='same')(input_img) 
    b1 = BatchNormalization()(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(b1) 
    conv2 = Conv2D(64, (5, 5), activation='relu', padding='same')(pool1) 
    b2 = BatchNormalization()(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(b2) 
    conv3 = Conv2D(128, (5, 5), activation='relu', padding='same')(pool2) 
    b3 = BatchNormalization()(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(b3)
    flat = Flatten()(pool3)
    den = Dense(256, activation='relu')(flat)
    den2= Dense(32, activation='relu')(den)
    out = Dense(2, activation='softmax')(den2)
    return out

def inputimg(shape):
    inChannel = shape[3]
    x, y = shape[1], shape[2]
    input_img = Input(shape = (x, y, inChannel))
    return input_img

def model_elab(input_img, checkpoint_filepath):
    full_model = Model(input_img,classifier(input_img))
    full_model.compile(loss="binary_crossentropy", optimizer=keras.optimizers.Adam(),metrics=['accuracy'])
    full_model.summary()

    model_checkpoint_callback = ModelCheckpoint(
        filepath=checkpoint_filepath,
        monitor='val_accuracy',
        mode='max',
        save_best_only=True
    )
    return full_model, model_checkpoint_callback

def model_load(full_model, checkpoint_filepath):
    full_model.load_weights(checkpoint_filepath)

def obtain_test_labels(test_OK, test_insp_OK, test_NOK):
    test_labels_OK = test_OK.shape[0]*[0]
    test_labels_insp_OK = test_insp_OK.shape[0]*[0]
    test_labels_NOK = test_NOK.shape[0]*[1]
    return test_labels_OK, test_labels_insp_OK, test_labels_NOK

def obtain_results(full_model, test_dataset, test_labels):
    predicted_classes = full_model.predict(test_dataset)
    predicted_classes = np.argmax(np.round(predicted_classes),axis=1)
    correct = np.where(predicted_classes==test_labels)[0]
    print("Found %d correct labels" % len(correct))
    incorrect = np.where(predicted_classes!=test_labels)[0]
    print ("Found %d incorrect labels" % len(incorrect))
    correct_percent = float(len(correct)/(len(correct) + len(incorrect))*100)
    incorrect_percent = float(len(incorrect)/(len(correct) + len(incorrect))*100)
    return correct_percent, incorrect_percent





def train_and_eval(defects_AA_folder, doubt_AA_folder, nondefects_AA_folder, train_percent, val_percent, test_percent, epochs, checkpoint_filepath):
    # AutoAugment: datos_OK - 6430 imgs / datos_insp_OK - 7524 imgs / datos_NOK - 5632 imgs
    # Originalmente: datos_OK - 643 imgs / datos_insp_OK - 3762 imgs / datos_NOK - 244 imgs
    datos_OK, datos_insp_OK, datos_NOK = classes_data(nondefects_AA_folder, doubt_AA_folder, defects_AA_folder)
    print (datos_OK.shape)
    print (datos_insp_OK.shape)
    print (datos_NOK.shape)

    # Normalizado del valor de los pixeles (0-1)
    datos_norm_OK, datos_norm_insp_OK, datos_norm_NOK = normalization(datos_OK, datos_insp_OK, datos_NOK)

    # Division de dataset en train, validation y test
    train_OK, val_OK, test_OK = train_val_test(datos_norm_OK, train_percent, val_percent, test_percent)
    train_insp_OK, val_insp_OK, test_insp_OK = train_val_test(datos_norm_insp_OK, train_percent, val_percent, test_percent)
    train_NOK, val_NOK, test_NOK = train_val_test(datos_norm_NOK, train_percent, val_percent, test_percent)
    print (train_OK.shape, val_OK.shape, test_OK.shape)
    print (train_insp_OK.shape, val_insp_OK.shape, test_insp_OK.shape)
    print (train_NOK.shape, val_NOK.shape, test_NOK.shape)

    # Obtencion de datos de entrenamiento y validacion, así como labels coherentes con entrada al modelo
    train_data, val_data, train_labels_one_hot, val_labels_one_hot = train_val_data(train_OK, train_insp_OK, train_NOK, val_OK, val_insp_OK, val_NOK, test_NOK)

    # Introducimos el input y elaboramos el modelo
    input_img = inputimg(train_data.shape)
    full_model, model_checkpoint_callback = model_elab(input_img, checkpoint_filepath)
    # Se hace la clasificacion con el modelo
    classify_train = full_model.fit(train_data, train_labels_one_hot, batch_size=25, epochs = epochs,verbose=1, callbacks=[model_checkpoint_callback], validation_data=(val_data, val_labels_one_hot))

    # Se carga el modelo entrenado
    model_load(full_model, checkpoint_filepath)

    # Se obtienen las etiquetas de los datos de test
    test_labels_OK, test_labels_insp_OK, test_labels_NOK = obtain_test_labels(test_OK, test_insp_OK, test_NOK)

    # Se muestran resultados de los datos de test de la clase OK
    correct_percent, incorrect_percent = obtain_results(full_model, test_OK, test_labels_OK)
    logger.debug(f'En los datos de test de la clase OK se ha obtenido una tasa de acierto de {correct_percent} y una tasa de fallo de {incorrect_percent}')
    # Se muestran resultados de los datos de test de la clase OK
    correct_percent, incorrect_percent = obtain_results(full_model, test_insp_OK, test_labels_insp_OK)
    logger.debug(f'En los datos de test de la clase insp_OK se ha obtenido una tasa de acierto de {correct_percent} y una tasa de fallo de {incorrect_percent}')
    # Se muestran resultados de los datos de test de la clase OK
    correct_percent, incorrect_percent = obtain_results(full_model, test_NOK, test_labels_NOK)
    logger.debug(f'En los datos de test de la clase NOK se ha obtenido una tasa de acierto de {correct_percent} y una tasa de fallo de {incorrect_percent}')


def eval_saved_model(defects_AA_folder, doubt_AA_folder, nondefects_AA_folder, train_percent, val_percent, test_percent, epochs, checkpoint_filepath):
    # AutoAugment: datos_OK - 6430 imgs / datos_insp_OK - 7524 imgs / datos_NOK - 5632 imgs
    # Originalmente: datos_OK - 643 imgs / datos_insp_OK - 3762 imgs / datos_NOK - 244 imgs
    datos_OK, datos_insp_OK, datos_NOK = classes_data(nondefects_AA_folder, doubt_AA_folder, defects_AA_folder)
    print (datos_OK.shape)
    print (datos_insp_OK.shape)
    print (datos_NOK.shape)

    # Normalizado del valor de los pixeles (0-1)
    datos_norm_OK, datos_norm_insp_OK, datos_norm_NOK = normalization(datos_OK, datos_insp_OK, datos_NOK)

    # Division de dataset en train, validation y test
    train_OK, val_OK, test_OK = train_val_test(datos_norm_OK, train_percent, val_percent, test_percent)
    train_insp_OK, val_insp_OK, test_insp_OK = train_val_test(datos_norm_insp_OK, train_percent, val_percent, test_percent)
    train_NOK, val_NOK, test_NOK = train_val_test(datos_norm_NOK, train_percent, val_percent, test_percent)
    print (train_OK.shape, val_OK.shape, test_OK.shape)
    print (train_insp_OK.shape, val_insp_OK.shape, test_insp_OK.shape)
    print (train_NOK.shape, val_NOK.shape, test_NOK.shape)

    # Obtencion de datos de entrenamiento y validacion, así como labels coherentes con entrada al modelo
    train_data, val_data, train_labels_one_hot, val_labels_one_hot = train_val_data(train_OK, train_insp_OK, train_NOK, val_OK, val_insp_OK, val_NOK, test_NOK)

    # Introducimos el input y elaboramos el modelo
    input_img = inputimg(train_data.shape)
    full_model, model_checkpoint_callback = model_elab(input_img, checkpoint_filepath)
    
    # Se carga el modelo entrenado
    model_load(full_model, checkpoint_filepath)

    # Se obtienen las etiquetas de los datos de test
    test_labels_OK, test_labels_insp_OK, test_labels_NOK = obtain_test_labels(test_OK, test_insp_OK, test_NOK)

    # Se muestran resultados de los datos de test de la clase OK
    correct_percent, incorrect_percent = obtain_results(full_model, test_OK, test_labels_OK)
    logger.debug(f'En los datos de test de la clase OK se ha obtenido una tasa de acierto de {correct_percent} y una tasa de fallo de {incorrect_percent}')
    # Se muestran resultados de los datos de test de la clase OK
    correct_percent, incorrect_percent = obtain_results(full_model, test_insp_OK, test_labels_insp_OK)
    logger.debug(f'En los datos de test de la clase insp_OK se ha obtenido una tasa de acierto de {correct_percent} y una tasa de fallo de {incorrect_percent}')
    # Se muestran resultados de los datos de test de la clase OK
    correct_percent, incorrect_percent = obtain_results(full_model, test_NOK, test_labels_NOK)
    logger.debug(f'En los datos de test de la clase NOK se ha obtenido una tasa de acierto de {correct_percent} y una tasa de fallo de {incorrect_percent}')


def obtain_augmented_folders(timestamp_saved, timestamp, augmentation_method, project_path):
    
    if augmentation_method.startswith('AA'):
        folder_middle = 'autoAugment_adapted'
    elif augmentation_method == 'FA':
        folder_middle = 'fastAugment_adapted'
    else:
        folder_middle = 'advchain_adapted'

    if timestamp_saved:
        second_folder = f'{augmentation_method}_{timestamp}'
    else:
        second_folder = f'{augmentation_method}_augmented_imgs'

    defects_folder = os.path.join(project_path, 'augmentation_processes', folder_middle, second_folder, 'defects')
    doubt_folder = os.path.join(project_path, 'augmentation_processes', folder_middle, second_folder, 'doubt')
    nondefects_folder = os.path.join(project_path, 'augmentation_processes', folder_middle, second_folder, 'non-defects')

    print (defects_folder)
    print (doubt_folder)
    print (nondefects_folder)
    return defects_folder, doubt_folder, nondefects_folder

    # defects_folder = '/home/luisarias/generator_project/augmentation_processes/autoAugment_adapted/AA_CF_2023-06-09_08:51:11/defects'
    # doubt_folder = '/home/luisarias/generator_project/augmentation_processes/autoAugment_adapted/AA_CF_2023-06-09_08:51:11/doubt'
    # nondefects_folder = '/home/luisarias/generator_project/augmentation_processes/autoAugment_adapted/AA_CF_2023-06-09_08:51:11/non-defects'
    # return defects_folder, doubt_folder, nondefects_folder

