## DESCRIPCIÓN GENERAL DEL SISTEMA
EL método de evaluación de las tres técnicas de aumento de datos procedentes de repositorios consiste en lo siguiente:
1. Se parte de la carpeta donde se encuentran guardadas las imágenes aumentadas y generadas mediante cada una de las tres técnicas trabajadas (AutoAugment, FastAugment y AdvChain).

2. Estas imágenes, que se presentan de manera balanceada, se dividen en los conjuntos de entrenamiento, validación y test. La división puede ser escogida por el usuario (el valor por defecto es 70-15-15).

3. Se entrena la red neuronal encargada de realizar esta clasificación de las distintas imágenes del dataset (se trata de una red neuronal convolucional de  poca profundidad). Así, se guardará un modelo en función del timestamp.

4. Se produce la evaluación del modelo obtenido sobre el conjunto de test.

NOTA: También se da la posibilidad de evaluar un modelo que ha sido guardado previamente. Así, se hará un división preeliminar del conjunto de datos aumentados inicial, y se realizará la evaluación sobre el conjunto de test.


## COMENTARIOS DE FUNCIONAMIENTO
Para que se dé un funcionamiento correcto, es necesario seguir los siguientes comandos:

1. Carpeta donde se encuentran los scripts de evaluación
```
cd /computer_vision_automation_project/evaluation_methods
```

2. Se lanza el script concreto
```
python3 train_eval.py
```

NOTA: Las variables que se van a modificar en el script en función de las necesidades del usuario son las siguientes:
- [ ] timestamp_saved: 
    - Si es 'True', se quiere entrenar con un conjunto de datos aumentado inicial del que se conoce el timestamp. 
    - Si es 'False', se quiere entrenar con uno de los datasets ya guardados por defectos (los cuales se utilizan en la elaboración del informe del proyecto)

- [ ] timestamp: 
    - En caso de timestamp_saved = 'True', se necesita copiar el timestamp que se desea de la carpeta concreta. 
    - En caso de timestamp_saved = 'False', no se hace caso a este parámetro

- [ ] augmentation_method: 
    - 'AA_CF' en caso de AutoAugment
    - 'FA' en caso de FastAugment
    - 'ADV' en caso de AdvChain

- [ ] train_percent, val_percent, test_percent: 

Porcentaje de división del conjunto de datos inicial en entrenamiento, validación y test

- [ ] epochs: 

Número de epochs con las que entrenar al modelo

- [ ] to_do: 
    - 'train_and_test' en caso de querer entrenar y evaluar a la vez (el modelo quedará guardado con un nombre en función del timestamp)
    - 'only_test' en caso de querer evaluar un modelo ya entrenado (se debe introducir el path del modelo guardado en la variable 'path_model_saved')

NOTA: Se han introducido los modelos utilizados en el informe (PDF a descargar), en las siguientes rutas:
- AutoAugment: [/computer_vision_automation_project/evaluation_methods/modelos_pruebas/AA_CF_best.h5](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/evaluation_methods/modelos_pruebas/AA_CF_best.h5?ref_type=heads)
- FastAugment: [/computer_vision_automation_project/evaluation_methods/modelos_pruebas/FA_best.h5](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/evaluation_methods/modelos_pruebas/FA_best.h5?ref_type=heads)
- AdvChain: [/computer_vision_automation_project/evaluation_methods/modelos_pruebas/ADV_best.h5](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/evaluation_methods/modelos_pruebas/ADV_best.h5?ref_type=heads)