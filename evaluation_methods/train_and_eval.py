from utils import *
import datetime
# Preprocesamiento
import tensorflow as tf
import os

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

# Project_path
project_path = os.path.dirname(os.getcwd())
print (project_path)

# Define variables
timestamp_saved = False
timestamp = '2023-06-09_08:51:11'
augmentation_method = 'ADV'
defects_folder, doubt_folder, nondefects_folder = obtain_augmented_folders(timestamp_saved, timestamp, augmentation_method, project_path)
train_percent = 0.7
val_percent = 0.15
test_percent = 0.15
epochs = 100
now = datetime.datetime.now()
timestamp = now.strftime("%Y-%m-%d_%H:%M:%S")
checkpoint_filepath = os.path.join(os.getcwd(), 'modelos_timestamp', f'{augmentation_method}_{timestamp}.h5')
to_do = 'only_test'
path_model_saved = os.path.join(os.getcwd(), 'modelos_pruebas', 'ADV_best.h5')
# path_model_saved = os.path.join(os.getcwd(), 'modelos_pruebas', 'AA_CF_best.h5')

if to_do == 'train_and_test':
    train_and_eval(defects_folder, doubt_folder, nondefects_folder, train_percent, val_percent, test_percent, epochs, checkpoint_filepath)
elif to_do == 'only_test':
    eval_saved_model(defects_folder, doubt_folder, nondefects_folder, train_percent, val_percent, test_percent, epochs, path_model_saved)