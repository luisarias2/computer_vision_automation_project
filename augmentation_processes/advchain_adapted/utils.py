# Importamos las librerías y dependencias necesarias
import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import torch
import shutil
import time
import datetime
from PIL import Image
from loguru import logger
from advchain.augmentor import ComposeAdversarialTransformSolver,AdvBias,AdvMorph,AdvNoise,AdvAffine
from advchain.models.unet import get_unet_model

def load_image(image_region, image, crop_size):
    if image_region == 'izq':
        cropped_image = load_image_izq(image, crop_size)
    else:
        cropped_image = load_image_der(image, crop_size)
    return cropped_image

def load_image_izq(image, crop_size):
    # print('image size:', image.shape)
    cropped_image = image[0:crop_size[0], 0:crop_size[1]]
    # rescale image intensities to 0-1
    cropped_image = (cropped_image-cropped_image.min()) / (cropped_image.max()-cropped_image.min()+1e-10)
    return cropped_image

def load_image_der(image, crop_size):
    # print('image size:', image.shape)
    cropped_image = image[0:crop_size[0], crop_size[1]:image.shape[1]]
    # rescale image intensities to 0-1
    cropped_image = (cropped_image-cropped_image.min()) / (cropped_image.max()-cropped_image.min()+1e-10)
    return cropped_image

def join_images(path_izq, path_der, concatenated_path):
    img_izq = cv2.imread(path_izq)
    img_der = cv2.imread(path_der)
    joined_img = cv2.hconcat([img_izq, img_der])
    cv2.imwrite(concatenated_path, joined_img)

def setup_transformations(crop_size):
    bs=1
    im_ch=1
    debug = False ## set debug to false to disable intermediate outputs
    augmentor_bias= AdvBias(
                    config_dict={'epsilon':0.3,
                    'control_point_spacing':[crop_size[0]//2,crop_size[1]//2],
                    'downscale':2,
                    'data_size':(bs,im_ch,crop_size[0],crop_size[1]),
                    'interpolation_order':3,
                    'init_mode':'random',
                    'space':'log'},
                    debug=debug)          

    augmentor_noise= AdvNoise( config_dict={'epsilon':1,
                    'xi':1e-6,
                    'data_size':(bs,im_ch,crop_size[0],crop_size[1])},
                    debug=debug)
        
    augmentor_affine= AdvAffine( config_dict={
                    'rot':30.0/180,
                    'scale_x':0.2,
                    'scale_y':0.2,
                    'shift_x':0.1,
                    'shift_y':0.1,
                    'data_size':(bs,im_ch,crop_size[0],crop_size[1]),
                    'forward_interp':'bilinear',
                    'backward_interp':'bilinear'},
                    image_padding_mode="reflection", ## change it other int/float values for padding or other padding mode like "reflection", "border" or "zeros," "zeros" is the default value
                    debug=debug)

    augmentor_morph= AdvMorph(
                    config_dict=
                    {'epsilon':1.5,
                    'data_size':(bs,im_ch,crop_size[0],crop_size[1]),
                    'vector_size':[crop_size[0]//16,crop_size[1]//16],
                    'forward_interp':'bilinear',
                    'backward_interp':'bilinear'
                    }, 
                    image_padding_mode="reflection", ## change it other int/float values for padding or other padding mode like "reflection", "border" or "zeros," "zeros" is the default value
                    debug=debug)
    
    return augmentor_noise, augmentor_bias, augmentor_morph, augmentor_affine


def obtain_advchain_transformed_image(image_path, crop_size, image_region, final_path):
    # Cargamos la imagen
    img_pil = Image.open(image_path)
    image = np.array(img_pil)
    print (type(image))
    print (f'La imagen original tiene las siguientes dimensiones: {image.shape}')


    # Cargamos el segmentation model
    model = get_unet_model(num_classes=4,model_path='./saved_checkpoints/cardiac_seg_unet_16.pth',model_arch='UNet_16')
    if torch.cuda.is_available() :
        use_gpu=True
        model = model.cuda()
    else:use_gpu=False
    model.eval()


    # Crop image para introduccion correcta del tensor en el modelo
    use_gpu = True
    # cropped_image =load_image(image, crop_size)
    cropped_image = load_image(image_region, image, crop_size)
    print (f'Tras la adaptacion, la nueva imagen tiene las siguientes dimensiones: {cropped_image.shape}')
    image_tensor = torch.from_numpy(cropped_image[np.newaxis,np.newaxis,:,:]).float()
    label_tensor = torch.from_numpy(cropped_image[np.newaxis,:,:]).long()
    if len(image_tensor.shape) == 5:
        image_tensor = image_tensor[:, :, :, :, -1]
        print ('Entro')
        print (f'Arreglo el error: {image_tensor.shape}')
    if use_gpu: 
        image_tensor = image_tensor.cuda()
        label_tensor = label_tensor.cuda()
    # init_output = model(image_tensor)

    # Setup de las transformaciones y evaluamos modelo
    augmentor_noise,augmentor_bias,augmentor_morph,augmentor_affine = setup_transformations(crop_size)
    model.eval()

    # Definicion de una cadena de transformaciones y especificacion del orden: noise->bias->morph->affine
    transformation_chain = [augmentor_noise,augmentor_bias,augmentor_morph,augmentor_affine] 

    # Setup de un solver para el Adversarial Data Augmentation
    solver = ComposeAdversarialTransformSolver(
            chain_of_transforms=transformation_chain,
            divergence_types = ['mse','contour'], ### you can also change it to 'kl'.
            divergence_weights=[1.0,0.5],
            use_gpu= True,
            debug=True,
            if_norm_image=True
        )

    # Inicializacion random
    slice_id=0
    solver.init_random_transformation()
    rand_transformed_image = solver.forward(image_tensor.detach().clone())
    rand_predict = solver.get_net_output(model,rand_transformed_image)
    warp_back_rand_predict= solver.predict_backward(rand_predict)
    rand_bias = augmentor_bias.bias_field
    rand_noise = augmentor_noise.param
    rand_dxy,rand_morph = augmentor_morph.get_deformation_displacement_field(-augmentor_morph.param)
    rand_deformed_image = augmentor_morph.transform((image_tensor+rand_noise)*rand_bias,rand_dxy, interp=augmentor_morph.forward_interp).cpu().data.numpy()[slice_id,0]

    # Funcion de perdida
    loss = solver.adversarial_training(
            data=image_tensor,model=model,
            n_iter=1,
            lazy_load=[True]*len(transformation_chain), ## if set to true, it will use the previous sampled random bias field as initialization.
            optimize_flags=[True]*len(transformation_chain),
            step_sizes=1,power_iteration=[False]*len(transformation_chain))    

    # Nos centramos en la imagen transformada
    adv_transformed_image = solver.forward(image_tensor.detach().clone())
    adv_transformed_image_numpy=adv_transformed_image.cpu().data.numpy()[slice_id,0]
    adv_transformed_image_numpy_new = (adv_transformed_image_numpy * 255).astype(np.uint8)
    print (type(adv_transformed_image_numpy_new))
    print (adv_transformed_image_numpy_new.shape)
    img = Image.fromarray(adv_transformed_image_numpy_new)
    img.save(final_path)


def folders_creation():
    '''
    Función para crear las carpetas fuente y destino (esta última en función de la política elegida)
    '''
    now = datetime.datetime.now()
    timestamp = now.strftime("%Y-%m-%d_%H:%M:%S")
    source_defects_folder = '/home/luisarias/generator_project/pixelDA/pixelDA_defect_images' 
    source_nondefects_folder = '/home/luisarias/generator_project/pixelDA/pixelDA_non-defect_images'
    source_doubt_folder = '/home/luisarias/generator_project/data_operations/doubt_images'
    source_list = [source_defects_folder, source_nondefects_folder, source_doubt_folder]
    destination_defects_folder = f'/home/luisarias/generator_project/augmentation_processes/advchain_adapted/{timestamp}/defects'
    destination_nondefects_folder = f'/home/luisarias/generator_project/augmentation_processes/advchain_adapted/{timestamp}/non-defects'
    destination_doubt_folder = f'/home/luisarias/generator_project/augmentation_processes/advchain_adapted/{timestamp}/doubt'
    destination_list = [destination_defects_folder, destination_nondefects_folder, destination_doubt_folder]
    for dest_folder in destination_list:
        if not os.path.isdir(dest_folder):
            os.makedirs(dest_folder)
    return source_list, destination_list


def copy_images(source_folder, target_folder): 
    '''
    Función para copiar imágenes de una carpeta a otra
    '''   
    for file_name in os.listdir(source_folder):
        global_file_path = os.path.join(source_folder, file_name)
        # Verificar si el archivo es una imagen (png, jpg, jpeg, bmp, gif)
        if file_name.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
            shutil.copy(global_file_path, target_folder)


def adv_one_image(global_image_path, crop_size, final_path):
    image_region_izq = 'izq'
    final_path_izq = f'/home/luisarias/advchain/adv1_{image_region_izq}.jpg'
    obtain_advchain_transformed_image(global_image_path, crop_size, image_region_izq, final_path_izq)
    image_region_der = 'der'
    final_path_der = f'/home/luisarias/advchain/adv1_{image_region_der}.jpg'
    obtain_advchain_transformed_image(global_image_path, crop_size, image_region_der, final_path_der)
    join_images(final_path_izq, final_path_der, final_path)
    os.remove(final_path_izq)
    os.remove(final_path_der)

def adv_augmentation(source_list, destination_list):
    '''
    Función para aplicar el adv augmentation
    '''
    # Implementamos el Data Augmentation
    for source_folder, destination_folder in zip(source_list, destination_list):
        # Copiamos las imágenes originales de la carpeta fuente a la carpeta destino
        copy_images(source_folder, destination_folder)
        # Para cada imagen presente en cada uno de los tipos de datasets, realizamos el adv augmentation
        tstart = time.time()
        for local_image_path in sorted(os.listdir(source_folder)):
            global_image_path = os.path.join(source_folder, local_image_path)
            print (global_image_path)
            crop_size = (256,256)
            final_path = os.path.join(destination_folder, f'{local_image_path.split(".")[0]}_adv.{local_image_path.split(".")[1]}')
            adv_one_image(global_image_path, crop_size, final_path)
        logger.debug(f'Se han tardado {time.time()-tstart} en realizar el Data Augmenation sobre la carpeta {source_folder.split("/")[-1]}')
        logger.debug(f'En dicha carpeta ahora hay {len(os.listdir(destination_folder))} imagenes')
        print ()

def adv_chain():
    source_list, destination_list = folders_creation()
    # Resultado del preprocesamiento balanceado: 2816 defectos - 3215 sin defectos - 3762 dudosas
    logger.debug(f'Se tienen {len(os.listdir(source_list[0]))} imágenes con defectos, {len(os.listdir(source_list[1]))} imágenes sin defectos y {len(os.listdir(source_list[2]))} imágenes dudosas')
    print ()
    adv_augmentation(source_list, destination_list)








    








