import os
import shutil
import time
import PIL
import datetime
import torch
from loguru import logger
from torchvision.transforms import ToTensor
from torchvision.transforms import ToPILImage
from fast_augment_torch import FastAugment

def copy_images(source_folder, target_folder): 
    '''
    Función para copiar imágenes de una carpeta a otra
    '''   
    for file_name in os.listdir(source_folder):
        global_file_path = os.path.join(source_folder, file_name)
        # Verificar si el archivo es una imagen (png, jpg, jpeg, bmp, gif)
        if file_name.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
            shutil.copy(global_file_path, target_folder)

def folders_creation():
    '''
    Función para crear las carpetas fuente y destino (esta última en función de la política elegida)
    '''
    now = datetime.datetime.now()
    timestamp = now.strftime("%Y-%m-%d_%H:%M:%S")
    source_defects_folder = '/home/luisarias/generator_project/pixelDA/pixelDA_defect_images' 
    source_nondefects_folder = '/home/luisarias/generator_project/pixelDA/pixelDA_non-defect_images'
    source_doubt_folder = '/home/luisarias/generator_project/data_operations/doubt_images'
    source_list = [source_defects_folder, source_nondefects_folder, source_doubt_folder]
    destination_defects_folder = f'/home/luisarias/generator_project/augmentation_processes/fastAugment_adapted/FA_{timestamp}/defects'
    destination_nondefects_folder = f'/home/luisarias/generator_project/augmentation_processes/fastAugment_adapted/FA_{timestamp}/non-defects'
    destination_doubt_folder = f'/home/luisarias/generator_project/augmentation_processes/fastAugment_adapted/FA_{timestamp}/doubt'
    destination_list = [destination_defects_folder, destination_nondefects_folder, destination_doubt_folder]
    for dest_folder in destination_list:
        if not os.path.isdir(dest_folder):
            os.makedirs(dest_folder)
    return source_list, destination_list, timestamp

def obtain_label(global_path):
    label = int(global_path.split("-")[1][0])
    return label

def fastAugment():
    source_list, destination_list, timestamp = folders_creation()
    for source_folder, destination_folder in zip(source_list, destination_list):
        print ('Empiezo')
        # Copiamos las imágenes originales de la carpeta fuente a la carpeta destino
        copy_images(source_folder, destination_folder)

        # Para cada imagen presente en cada uno de los tipos de datasets, realizamos el FastAugment
        tstart = time.time()
        x = []
        y = []
        for local_image_path in sorted(os.listdir(source_folder)):
            global_image_path = os.path.join(source_folder, local_image_path)
            label = obtain_label(local_image_path)
            image_pil = PIL.Image.open(global_image_path)
            # tensor_img = to_grayscale(image_pil)
            transform = ToTensor()
            tensor_img = transform(image_pil)
            # Todos los tensores con esta dimension [256, 512, 3] para FastAugment
            tensor_img = tensor_img.expand(3, -1, -1)
            x.append(tensor_img.cuda().permute(1, 2, 0).contiguous())
            y.append(torch.nn.functional.one_hot(torch.LongTensor([label]), num_classes=3))
        x = torch.stack(x, dim=0)
        print (f'x tiene la siguiente forma: {x.shape}')
        y = torch.cat(y).to(torch.float32)
        print (f'y tiene la siguiente forma: {y.shape}')

        # Me creo una lista de tensores, los cuales no pueden sobrepasar el "texture height" de 65536
        # Es decir, los tensores deben tener una longitud menor a 256 imágenes
        longitud_fragmentos = 100
        x_fragmented = torch.chunk(x, int(x.shape[0]/longitud_fragmentos), dim=0)
        y_fragmented = torch.chunk(y, int(y.shape[0]/longitud_fragmentos), dim=0)
        print (len(x_fragmented))
        print (x_fragmented[0].shape)
        print (len(y_fragmented))
        print (y_fragmented[0].shape)
        local_path_list = sorted(os.listdir(source_folder))
        local_path_fragmented = [local_path_list[i:i+longitud_fragmentos] for i in range(0, len(local_path_list), longitud_fragmentos)]

        x_augmented_list = []
        y_augmented_list = []
        for (x,y) in zip(x_fragmented, y_fragmented):
            # augment = FastAugment(translation=0, scale=0, rotation=0, perspective=0, brightness=0, color_inversion=False, cutout=0)
            augment = FastAugment(translation=0, perspective=0, scale=0, cutout=0)
            x, y = augment(x, y)
            x_augmented_list.append(x)
            y_augmented_list.append(y)

        torch.cuda.empty_cache()

        for (x, local_list) in zip(x_augmented_list, local_path_fragmented):
            for (i, local_image_path) in zip(range(len(x)), local_list):
                tensor_img = x[i].permute(2, 0, 1)
                transform = ToPILImage()
                img = transform(tensor_img)
                title = f'{local_image_path.split(".")[0]}_FastAugment.{local_image_path.split(".")[1]}'
                final_path = os.path.join(destination_folder, title)
                img.save(final_path)

        logger.debug(f'Se han tardado {time.time()-tstart} en realizar el Data Augmentation sobre la carpeta {source_folder.split("/")[-1]}')
        logger.debug(f'En dicha carpeta ahora hay {len(os.listdir(destination_folder))} imagenes')
        print ()
        logger.debug(f'El timestamp de la carpeta, para posteriores entrenamientos, es el siguiente: {timestamp}')
        