from autoaugment import CIFAR10Policy, ImageNetPolicy, SVHNPolicy
import os
import shutil
import time
import PIL
import datetime
from loguru import logger

def copy_images(source_folder, target_folder): 
    '''
    Función para copiar imágenes de una carpeta a otra
    '''   
    for file_name in os.listdir(source_folder):
        global_file_path = os.path.join(source_folder, file_name)
        # Verificar si el archivo es una imagen (png, jpg, jpeg, bmp, gif)
        if file_name.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
            shutil.copy(global_file_path, target_folder)
        

def folders_creation(policy):
    '''
    Función para crear las carpetas fuente y destino (esta última en función de la política elegida)
    '''
    now = datetime.datetime.now()
    timestamp = now.strftime("%Y-%m-%d_%H:%M:%S")
    source_defects_folder = '/home/luisarias/generator_project/pixelDA/pixelDA_defect_images' 
    source_nondefects_folder = '/home/luisarias/generator_project/pixelDA/pixelDA_non-defect_images'
    source_doubt_folder = '/home/luisarias/generator_project/data_operations/doubt_images'
    source_list = [source_defects_folder, source_nondefects_folder, source_doubt_folder]
    destination_defects_folder = f'/home/luisarias/generator_project/augmentation_processes/autoAugment_adapted/AA_{policy}_{timestamp}/defects'
    destination_nondefects_folder = f'/home/luisarias/generator_project/augmentation_processes/autoAugment_adapted/AA_{policy}_{timestamp}/non-defects'
    destination_doubt_folder = f'/home/luisarias/generator_project/augmentation_processes/autoAugment_adapted/AA_{policy}_{timestamp}/doubt'
    destination_list = [destination_defects_folder, destination_nondefects_folder, destination_doubt_folder]
    for dest_folder in destination_list:
        if not os.path.isdir(dest_folder):
            os.makedirs(dest_folder)
    return source_list, destination_list

def obtain_policy(policy):
    '''
    Función para obtener el objeto "policy" adecuado
    '''
    if policy == 'CF':
        policy = CIFAR10Policy()
    elif policy == 'IN':
        policy = ImageNetPolicy()
    elif policy == 'SVHN':
        policy = SVHNPolicy()
    else:
        print ('Politica no existente')
    return policy

def AutoAugment(pol, source_list, destination_list):
    '''
    Función para aplicar el AutoAugment en función de la política escogida
    '''
    policy = obtain_policy(pol)
    # Implementamos el Data Augmentation
    for source_folder, destination_folder in zip(source_list, destination_list):
        # Copiamos las imágenes originales de la carpeta fuente a la carpeta destino
        copy_images(source_folder, destination_folder)
        # Para cada imagen presente en cada uno de los tipos de datasets, realizamos el AutoAugment
        tstart = time.time()
        for local_image_path in sorted(os.listdir(source_folder)):
            global_image_path = os.path.join(source_folder, local_image_path)
            image = PIL.Image.open(global_image_path)
            transformed = policy(image)
            final_path = os.path.join(destination_folder, f'{local_image_path.split(".")[0]}_AutoAugment.{local_image_path.split(".")[1]}')
            transformed.save(final_path)
        logger.debug(f'Se han tardado {time.time()-tstart} en realizar el Data Augmentation sobre la carpeta {source_folder.split("/")[-1]}')
        logger.debug(f'En dicha carpeta ahora hay {len(os.listdir(destination_folder))} imagenes')
        print ()