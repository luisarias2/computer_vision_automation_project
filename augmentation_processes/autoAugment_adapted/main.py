from utils import *
from loguru import logger

# Posibles políticas a aplicar: 'CF', 'IN', 'SVHN'
policy = 'CF'
source_list, destination_list = folders_creation(policy)
# Resultado del preprocesamiento balanceado: 2816 defectos - 3215 sin defectos - 3762 dudosas
logger.debug(f'Se tienen {len(os.listdir(source_list[0]))} imágenes con defectos, {len(os.listdir(source_list[1]))} imágenes sin defectos y {len(os.listdir(source_list[2]))} imágenes dudosas')
print ()
AutoAugment(policy, source_list, destination_list)

