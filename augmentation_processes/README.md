## DESCRIPCIÓN GENERAL DEL SUBSISTEMA
Esta carpeta se utiliza para realizar el segundo procedimiento de aumento de datos, tras haber aplicado previamente operaciones pixelares y haber obtenido un nuevo conjunto de datos más balanceado, pero que sigue necesitando ser ampliado en cantidad de muestras.

Se aplican de manera distinta tres procedimientos de aumento de datos diferentes:
- [ ] AutoAugment: [/computer_vision_automation_project/augmentation_processes/autoAugment_adapted](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes/autoAugment_adapted?ref_type=heads)
- [ ] FastAugment: [/computer_vision_automation_project/augmentation_processes/fastAugment_adapted](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes/fastAugment_adapted?ref_type=heads)
- [ ] AdvChain: [/computer_vision_automation_project/augmentation_processes/advchain_adapted](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes/advchain_adapted?ref_type=heads)

El funcionamiento y las premisas teóricas fundamentales de cada uno de ellos viene detallada de manera exhaustiva en el apartado del Contexto del documento del proyecto (PDF a descargar). 

De esta manera, se generan las tres siguientes carpetas, correspondientes a la aplicación directa de cada una de las tres técnicas de respositorios mencionadas:
[/computer_vision_automation_project/augmentation_processes/autoAugment_adapted/AA_CF_augmented_imgs](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes/autoAugment_adapted/AA_CF_augmented_imgs?ref_type=heads)
[/computer_vision_automation_project/augmentation_processes/fastAugment_adapted/FA_augmented_imgs](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes/fastAugment_adapted/FA_augmented_imgs?ref_type=heads)
[/computer_vision_automation_project/augmentation_processes/advchain_adapted/ADV_augmented_imgs](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/augmentation_processes/advchain_adapted/ADV_augmented_imgs?ref_type=heads)

En cada una de estas carpetas, se tiene la misma distribución de imágenes:
![Imágenes presentes en cada una de las etiquetas](/imagenes/ConjuntoDatos_AfterRepos.png)

## COMENTARIOS DE FUNCIONAMIENTO
Para que se dé un funcionamiento correcto, es necesario seguir los siguientes comandos:

1. En función de la técnica a aplicar:

    1.1. AutoAugment
    ```
    cd /computer_vision_automation_project/augmentation_processes/autoAugment_adapted
    ```

    1.2. FastAugment
    ```
    cd /computer_vision_automation_project/augmentation_processes/fastAugment_adapted
    ```

    1.3. AdvChain
    ```
    cd /computer_vision_automation_project/augmentation_processes/advchain_adapted
    ```

2. Ejecución del main presente en la carpeta seleccionada:
```
python3 main.py
```
