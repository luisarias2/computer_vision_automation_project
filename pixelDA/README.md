## DESCRIPCIÓN GENERAL DEL SISTEMA
Esta carpeta se utiliza para realizar una primera operación de Data Augmentation, consistente en la traslación de los distintos fragmentos de la imagen.
Se puede considerar un aumento de datos manual, el cual se va a aplicar sobre las imágenes de partida con etiquetas '0' y '2' (mapas de saliencia sin defectos y mapas de saliencia con defectos).

Se van a presentar las dos alternativas que se ofrecen:

1. Transformaciones individuales y customizadas, donde se tienen en cuenta los siguientes parámetros fundamentalmente:
- [ ] folder_choice: 

Carpeta sobre la cual se quieren hacer las transformaciones (Opciones: 'defects' y 'non_defects') 

- [ ] pixel_delta: 

Número de píxeles de cada uno de los fragmentos de traslación de la imagen

- [ ] axis: 

Eje sobre el cual se va a realizar la traslación

2. Transformaciones por defecto, que son las que se realizan en el proyecto con el objetivo de presentar un dataset inicial algo balanceado. Las operaciones que se realizan son las siguientes:

- Traslaciones sobre ambos ejes, de 64 píxeles sobre el conjunto de datos con etiqueta '2' (mapas de saliencia con defectos).
- Traslaciones de 128 píxeles sobre el conjunto de datos con etiqueta '0' (mapas de saliencia sin defectos). 

Esta segunda opción se ejecuta automáticamente en la función "default_transformation". Así, partiendo de 256 imágenes con defectos y 643 imágenes sin defectos, se obtienen finalmente:
- 2816 imágenes con defectos: 7 transformaciones extra eje X, 3 transformaciones extra eje Y.

![Operaciones pixelares en mapas de saliencia con defectos](/imagenes/Pixel_DA_defects.png)

- 3215 imágenes sin defectos: 3 transformaciones extra eje X, 1 transformación extra eje Y.

![Operaciones pixelares en mapas de saliencia sin defectos](/imagenes/Pixel_DA_nondefects.png)

Así, con esta transformación por defecto, se tendrán las tres siguientes carpetas, con las que se trabajará posteriormente:
- MS sin defectos: 3215 imágenes [/computer_vision_automation_project/pixelDA/pixelDA_non-defect_images](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/pixelDA/pixelDA_non-defect_images?ref_type=heads) 
- MS dudosos: 3762 imágenes [/computer_vision_automation_project/data_operations/doubt_images](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/data_operations/doubt_images?ref_type=heads) 
- MS con defectos: 2816 imágenes [/computer_vision_automation_project/pixelDA/pixelDA_defect_images](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/pixelDA/pixelDA_defect_images?ref_type=heads)

## COMENTARIOS DE FUNCIONAMIENTO
Para que se dé un funcionamiento correcto, es necesario seguir los siguientes comandos:

Opción 1: Transformaciones individuales y customizadas
```
cd /home/luisarias/generator_project/pixelDA
```
```
python3 particular_pixelDA.py
```

NOTA: previamente hay que ajustar los parámetros de 'folder_choice', 'pixel_delta' y 'axis' en función de las necesidades. Finalmente, se creará una carpeta temporal (cuyo final del nombre es el timestamp, donde se encontrarán las imágenes pedidas en este momento puntual).

Opción 2: Transformaciones por defecto
```
cd /home/luisarias/generator_project/pixelDA
```
```
python3 pixelDA.py
```
 
