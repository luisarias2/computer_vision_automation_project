# PATH - /home/luisarias/PREPROCESSING/pixelDA
from loguru import logger
import numpy as np
import os
import shutil
import cv2
import datetime

# Copy function
def copying_images(input_path, output_path):
    for local_path in os.listdir(input_path):        
        shutil.copy(os.path.join(input_path, local_path), output_path)

# Translation function
def translation_DA(pixel_delta, axis, output_folder, output_path, input_path):
    '''
    This function is used to perform these tasks:
    1. Copy all original images from input folder (located in input path) to the output folder (located in the output path).
    2. Applies the specific shift transformations depending on pixel_delta and axis values. All resulted images will be stored in the output folder, located in output path.
    '''
    # 1. Copy original images in the output folder
    copying_images(input_path, output_path)

    # 2. Specific translation regarding 'pixel_delta' and 'axis'
    # Working on each image present in the output folder
    for path in os.listdir(output_folder):
        current_image_path = os.path.join(output_path, path)
        print (current_image_path)
        old_image = cv2.imread(current_image_path)
        size = old_image.shape
        # Different shape index depending on 'axis' values
        if axis=='X':
            shape_element = 1
        else:
            shape_element = 0
        # Shift transformations 
        for i in range(int(size[shape_element]/pixel_delta)-1):
            new_img = np.roll(old_image, pixel_delta, shape_element)
            path_no_extension = path.split('.')[0]
            label_extension = path.split('.')[1] 
            new_path = os.path.join(output_path, f'{path_no_extension}_{pixel_delta}_{axis}_{i}.{label_extension}')
            print (new_path)
            print ()
            cv2.imwrite(new_path, new_img)
            old_image = new_img

def pixelDA_setup():
    divided_dataset_path = '/home/luisarias/generator_project/data_operations'
    defects_folder = 'defect_images'
    defects_path = os.path.join(divided_dataset_path, defects_folder)
    logger.debug(f"Hay un total de {len(os.listdir(defects_path))} imágenes con defectos")
    non_defects_folder = 'non-defect_images'
    non_defects_path = os.path.join(divided_dataset_path, non_defects_folder)
    logger.debug(f"Hay un total de {len(os.listdir(non_defects_path))} imágenes sin defectos")
    return defects_folder, defects_path, non_defects_folder, non_defects_path

def outputs_depending_choice(folder_choice, defects_folder, defects_path, non_defects_folder, non_defects_path):
    if folder_choice == 'defects':
        input_folder = defects_folder
        input_path = defects_path
    else:
        input_folder = non_defects_folder
        input_path = non_defects_path
    output_folder = f'auxDA_{input_folder}'
    output_path = os.path.join(os.getcwd(), output_folder)
    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)
    return input_folder, input_path, output_folder, output_path

def copy_final_folder(input_folder, output_folder, output_path):
    final_folder = f'pixelDA_{input_folder}'
    if not os.path.isdir(final_folder):
        os.mkdir(final_folder)
    final_path = os.path.join(os.getcwd(), final_folder)
    copying_images(output_path, final_path)
    shutil.rmtree(output_folder)
    logger.debug(f"Hay un total de {len(os.listdir(final_path))} imágenes con defectos tras el Data Augmentation manual")

def verify_lengths():
    initial_defects = '/home/luisarias/generator_project/data_operations/defect_images'
    initial_doubt = '/home/luisarias/generator_project/data_operations/doubt_images'
    initial_non_defects = '/home/luisarias/generator_project/data_operations/non-defect_images'
    final_defects = '/home/luisarias/generator_project/pixelDA/pixelDA_defect_images'
    final_doubt = initial_doubt
    final_non_defects = '/home/luisarias/generator_project/pixelDA/pixelDA_non-defect_images'
    logger.debug(f'Originalmente, habia {len(os.listdir(initial_defects))}, {len(os.listdir(initial_doubt))} y {len(os.listdir(initial_non_defects))} imagenes con defectos, dudosas y sin defectos respectivamente')
    logger.debug(f'Tras el DataAugmentation manual, hay {len(os.listdir(final_defects))}, {len(os.listdir(final_doubt))} y {len(os.listdir(final_non_defects))} imagenes con defectos, dudosas y sin defectos respectivamente')

def default_transformation():
    defects_folder, defects_path, non_defects_folder, non_defects_path = pixelDA_setup()
    input_folder, input_path, output_folder, output_path = outputs_depending_choice('defects', defects_folder, defects_path, non_defects_folder, non_defects_path)
    translation_DA(64, 'X', output_folder, output_path, input_path)
    copy_final_folder(input_folder, output_folder, output_path)
    input_folder, input_path, output_folder, output_path = outputs_depending_choice('defects', defects_folder, defects_path, non_defects_folder, non_defects_path)
    translation_DA(64, 'Y', output_folder, output_path, input_path)
    copy_final_folder(input_folder, output_folder, output_path)
    input_folder, input_path, output_folder, output_path = outputs_depending_choice('non_defects', defects_folder, defects_path, non_defects_folder, non_defects_path)
    translation_DA(128, 'X', output_folder, output_path, input_path)
    copy_final_folder(input_folder, output_folder, output_path)
    input_folder, input_path, output_folder, output_path = outputs_depending_choice('non_defects', defects_folder, defects_path, non_defects_folder, non_defects_path)
    translation_DA(128, 'Y', output_folder, output_path, input_path)
    copy_final_folder(input_folder, output_folder, output_path)
    print ()
    verify_lengths()

def copy_temporal_folder(input_folder, output_folder, output_path):
    now = datetime.datetime.now()
    timestamp = now.strftime("%Y-%m-%d_%H:%M:%S")
    final_folder = f'pixelDA_{input_folder}_{timestamp}'
    if not os.path.isdir(final_folder):
        os.mkdir(final_folder)
    final_path = os.path.join(os.getcwd(), final_folder)
    copying_images(output_path, final_path)
    shutil.rmtree(output_folder)
    logger.debug(f"Hay un total de {len(os.listdir(final_path))} imágenes con defectos tras el Data Augmentation manual")
