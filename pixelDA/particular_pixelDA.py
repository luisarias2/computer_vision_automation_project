from functions_pixelDA import translation_DA, pixelDA_setup, outputs_depending_choice, copy_temporal_folder

# Setup
defects_folder, defects_path, non_defects_folder, non_defects_path = pixelDA_setup()
folder_choice = 'non_defects'
pixel_delta = 128
axis = 'Y'
# Translation transformation
input_folder, input_path, output_folder, output_path = outputs_depending_choice(folder_choice, defects_folder, defects_path, non_defects_folder, non_defects_path)
translation_DA(pixel_delta, axis, output_folder, output_path, input_path)
copy_temporal_folder(input_folder, output_folder, output_path)