from PIL import Image
import os

# Directorio global que contiene las carpetas train, validation y test
yolov8_folder = os.getcwd()
directorio_global = os.path.join(yolov8_folder, "background_22", "Dataset_Clasificacion")

# Tamaño de destino para redimensionar las imágenes
nuevo_ancho = 256
nuevo_alto = 256

# Función para redimensionar una imagen
def redimensionar_imagen(ruta_imagen, nuevo_ancho, nuevo_alto):
    imagen = Image.open(ruta_imagen)
    imagen_redimensionada = imagen.resize((nuevo_ancho, nuevo_alto))
    return imagen_redimensionada

# Recorrer los directorios y redimensionar las imágenes
for division in ['train', 'val', 'test']:
    division_directorio = os.path.join(directorio_global, division)
    for subcarpeta in ['defects', 'non_defects']:
        subcarpeta_directorio = os.path.join(division_directorio, subcarpeta)
        for imagen_nombre in os.listdir(subcarpeta_directorio):
            imagen_ruta = os.path.join(subcarpeta_directorio, imagen_nombre)
            imagen_redimensionada = redimensionar_imagen(imagen_ruta, nuevo_ancho, nuevo_alto)
            
            # Guardar la imagen redimensionada (puedes ajustar la ruta según necesites)
            # nueva_ruta = os.path.join(subcarpeta_directorio, imagen_nombre.replace('.png', '_resized.png'))
            imagen_redimensionada.save(imagen_ruta)
