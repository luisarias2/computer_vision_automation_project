from ultralytics import YOLO
from loguru import logger
import os
import shutil

# ClearML Communication
from clearml import Task
nombre_proyecto = "defects classification"
task = Task.init(project_name="Defects Classification with Fast Augmented dataset", task_name=nombre_proyecto)


# Se escoge si se desea hacer entrenamiento o clasificación
train = False
test = False
predict = False
accion = input(f'Eleccion de entrenamiento o test: \n Entrenamiento: TR \n Test: TS \n Predict: P \n')
while True:
    if accion == 'TR':
        train= True
        break
    elif accion == 'TS':
        test= True
        break
    elif accion == 'P':
        predict = True
        break
    else:
        logger.debug('No se han escrito las instrucciones de manera adecuada')
        accion = input(f'Eleccion de entrenamiento o test: \n Entrenamiento: TR \n Test: TS \n')
    

# Entrenamiento
if train:
    # Path de los datos
    data_path = "/home/luisarias/generator_project/yolov8_classification_share/background22/Dataset_Clasificacion"
    # # Tamaño del modelo de clasificacion de YOLOV8
    # model_size = input('Modelo de YOLOV8 con el que entrenar: n, s, m, l ó x')
    # print(f'poniendo el modelo {model_size}')
    model = YOLO("yolov8x-cls.pt")  # load a pretrained model (recommended for training)
    # Tamaño del batch y de la imagen      
    batch = int(input(f'Tamaño de batch a utilizar\n'))
    logger.debug(f'El batch se ha ajustado a {batch}')
    imgsize = int(input(f'tamaño del imagen , p.e 640\n'))
    print(f'La imagen se ha ajustado a un tamaño de {imgsize} pixeles')
    # Entrenamiento del modelo
    model.train(data=data_path, 
                epochs=1000, 
                batch = batch,
                patience= 10,
                imgsz=imgsize, 
                save=True, 
                # save_period=-1, 
                # cache=False, 
                #device=None, 
                #workers=8, 
                #project=None, 
                name=nombre_proyecto, 
                #exist_ok=False, 
                pretrained=True, 
                #optimizer=auto, 
                #verbose=True, 
                #seed=0, 
                #deterministic=True, 
                #single_cls=False, 
                #rect=False, 
                #cos_lr=False, 
                #close_mosaic=10, 
                resume=False, 
                #amp=True, 
                #fraction=1.0,
                #profile=False, 
                #overlap_mask=True, 
                #mask_ratio=4, 
                #dropout=0.0, 
                #val=True, 
                #split=val, 
                #save_json=False, 
                #save_hybrid=False, 
                #conf=None,
                #iou=0.7, 
                #max_det=300, 
                #half=False, 
                #dnn=False, 
                #plots=True, 
                #source=None, 
                show=False, 
                #save_txt=False,
                #save_conf=False, 
                #save_crop=False, 
                #show_labels=True, 
                #show_conf=True, 
                #vid_stride=1, 
                #line_width=None,
                #visualize=False, 
                #augment=False,
                agnostic_nms=True, 
                #classes=None, 
                #retina_masks=False,
                #boxes=True, 
                #format=torchscript, 
                #keras=False, 
                #optimize=False,
                #int8=False, 
                #dynamic=False,
                #simplify=False,
                #opset=None, 
                #workspace=4,
                nms=True, 
                #lr0=0.01, 
                #lrf=0.01, 
                #momentum=0.937, 
                #weight_decay=0.0005, 
                #warmup_epochs=3.0,
                #warmup_momentum=0.8, 
                #warmup_bias_lr=0.1, 
                #box=7.5, 
                #cls=0.5, 
                #dfl=1.5, 
                #pose=12.0, 
                #kobj=1.0, 
                #label_smoothing=0.0, 
                #nbs=64, 
                # hsv_h=0.015, 
                # hsv_s=0.7,
                # hsv_v=0.4, 
                # degrees=0.0, 
                # translate=0.1, 
                # scale=0.5, 
                # shear=0.001, 
                # perspective=0.001,
                # flipud=0.0, 
                # fliplr=0.5, 
                # mosaic=0.5, 
                # mixup=0.0, 
                # copy_paste=0.0,
                #cfg=None, 
                # v5loader=False, 
                #tracker=botsort.yaml
                )  # train the model

# Test
if test:
    data_path = "/home/luisarias/generator_project/yolov8_classification_share/background22/Dataset_Clasificacion"
    weights_path = "/home/luisarias/generator_project/yolov8_classification_share/background22/best_background22.pt"
    test_model = YOLO(weights_path)
    test_model.val(data=data_path, 
                   split = 'test')

# Validation  
if predict:
    counter = 0
    print ('Entro')
    images_folder_path = '/home/luisarias/generator_project/yolov8_classification_share/background22/Test_out/non-defects_resize_200'
    model_path = '/home/luisarias/generator_project/yolov8_classification_share/background22/best_background22.pt'
    print (model_path)
    model = YOLO(model_path)
    print (model)
    for image in os.listdir(images_folder_path):
        results = model(os.path.join(images_folder_path, image))
        print (results[0].names)
        print (results[0].probs.data.tolist())
        if int(results[0].probs.data.tolist()[0]<results[0].probs.data.tolist()[1]):
            counter += 1
        else:
            print (image)
    print (f'Acierto: {counter}/{len(os.listdir(images_folder_path))}')

