from PIL import Image
import os

# Directorio global que contiene las carpetas train, validation y test
yolov8_folder = os.getcwd()
directorio_original = os.path.join(yolov8_folder, "background22", "Test_out", "defects_resized_200")
# directorio_resized = '/home/training/yolov8_classification_share/background_22/new_test22/non-defects_resized'
# if not os.path.exists(directorio_resized):
#     os.makedirs(directorio_resized)

# Tamaño de destino para redimensionar las imágenes
nuevo_ancho = 256
nuevo_alto = 256

# Función para redimensionar una imagen
def redimensionar_imagen(ruta_imagen, nuevo_ancho, nuevo_alto):
    imagen = Image.open(ruta_imagen)
    imagen_redimensionada = imagen.resize((nuevo_ancho, nuevo_alto))
    return imagen_redimensionada

# Recorrer los directorios y redimensionar las imágenes
for imagen_nombre in os.listdir(directorio_original):
    print (imagen_nombre)
    imagen_ruta = os.path.join(directorio_original, imagen_nombre)
    imagen_redimensionada = redimensionar_imagen(imagen_ruta, nuevo_ancho, nuevo_alto)
    
    # Guardar la imagen redimensionada (puedes ajustar la ruta según necesites)
    # nueva_ruta = os.path.join(directorio_resized, imagen_nombre)
    nueva_ruta = imagen_ruta
    imagen_redimensionada.save(nueva_ruta)
