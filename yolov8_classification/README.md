## DESCRIPCIÓN GENERAL DEL SISTEMA
Esta carpeta se utiliza para el entrenamiento, test y predicción de un clasificador YOLOv8 de garantía sobre un conjunto de datos que ha sido seleccionado tras la evaluación de las tecnicas de aumento de datos basadas en repositorios (los criterios seguidos se pueden observar en el apartado de Metodología del proyecto). 

Dicho dataset se ha almacenado en la siguiente carpeta: [/computer_vision_automation_project/yolov8_classification/background22/Dataset_global](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/yolov8_classification/background22/Dataset_global?ref_type=heads) 

Además, la estructura de este se presenta en la siguiente figura.
![Conjunto de datos empleado para la clasificación binaria final](/imagenes/ConjuntoDatos_YOLOv8.png)

Antes de cualquier tipo de operación, es necesario tener en cuenta ciertos criterios en el diseño del entrenamiento para el proyecto:

1. Con el objetivo de replicar un caso de uso real, se van a seleccionar 200 imágenes de cada clase que van a ser completamente ajenas al procedimiento de entrenamiento, validación o test del clasificador. De esta manera, servirán como ejemplo de escenario de producción para evaluar verdaderamente si el clasificador obtenido podría servir como nueva herramienta para el proceso de inspección de la calidad.
- 200 mapas de saliencia con defectos: [/computer_vision_automation_project/yolov8_classification/background22/Test_out/non-defects_resized_200](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/yolov8_classification/background22/Test_out/non-defects_resize_200?ref_type=heads)
- 200 mapas de saliencia sin defectos: [/computer_vision_automation_project/yolov8_classification/background22/Test_out/defects_resize_200](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/yolov8_classification/background22/Test_out/defects_resized_200?ref_type=heads)

2. Cada una de las clases del dataset se divide en conjuntos de entrenamiento, validación y test. En el proyecto, se ha escogido una división estándar de 70-15-15.
- [ ] Script de generación automática del dataset global al de clasificación: [/computer_vision_automation_project/yolov8_classification/dataset_classification.py](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/yolov8_classification/dataset_classification.py?ref_type=heads)
- [ ] Conjunto de datos de clasificación generado (con sus respectivas subcarpetas): [/computer_vision_automation_project/yolov8_classification/background22/Dataset_Clasificacion](https://gitlab.com/luisarias2/computer_vision_automation_project/-/tree/version0.2/yolov8_classification/background22/Dataset_Clasificacion?ref_type=heads)

3. Se realiza una operación de resize de todas las imágenes, ya que el entrenamiento de YOLO se realiza con imágenes cuadradas (square images). En el proyecto, las imágenes presentan un tamaño original de 512x256 (ancho-alto), por lo que se elige comprimir toda la información de la imagen en 256x256. Para ello, se cuenta con dos scripts:
- Resize de todas las imágenes del Dataset de Clasificación (train, val, test): [/computer_vision_automation_project/yolov8_classification/resize_clasification_dataset.py](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/yolov8_classification/resize_classification_dataset.py?ref_type=heads)
- Resize de todas las imágenes de una carpeta (para las 200 de cada clase ajenas al entrenamiento): [/computer_vision_automation_project/yolov8_classification/resize_one_folder.py](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/yolov8_classification/resize_one_folder.py?ref_type=heads)

4. Se elige el modelo YOLOv8x-cls, para ser entrenado con el conjunto de datos del proyecto, ya que es el que mejores resultados en términos de precisión presenta sobre el dataset de ImageNet, tal y como se observa en la figura siguiente.
- Ruta del modelo: [/computer_vision_automation_project/yolov8_classification/yolov8x-cls.pt](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/yolov8_classification/yolov8x-cls.pt?ref_type=heads)

![Modelos de clasificación pre-entrenados en ImageNet](/imagenes/Modelos_Clasificacion.png)

5. Se entrena el modelo de clasificación con la arquitectura YOLOv8, y resulta un modelo nuevo.
- Ruta del modelo clasificador para el proyecto: [/computer_vision_automation_project/yolov8_classification/background22/yolo_classifier.pt](https://gitlab.com/luisarias2/computer_vision_automation_project/-/blob/version0.2/yolov8_classification/background22/yolo_classifier.pt?ref_type=heads)

6. Dicho modelo clasificador resultante, se utiliza para predecir en un escenario de operación, con las 200 imágenes seleccionadas en el primer paso.

## COMENTARIOS DE FUNCIONAMIENTO
Para que se dé un funcionamiento correcto, es necesario seguir los siguientes comandos:

1. Preprocesamiento y tareas previas (pasos 1-4)
```
cd /computer_vision_automation_project/yolov8_classification
```

Para generar el conjunto de datos de clasificación
```
python3 dataset_classification.py 
```

Para hacer resize sobre el conjunto de datos completo de clasificación
```
python3 resize_classification_dataset.py 
```

Para hacer resize sobre los datos presentes en una carpeta concreta
```
python3 resize_one_folder.py 
```

2. Entrenamiento, test y predicción (pasos 5 y 6)
```
cd /computer_vision_automation_project/yolov8_classification
```
```
python3 yolo_model_operations.py
```

NOTA: En la ejecución de este script, se van a pedir una serie de "inputs" por pantalla, que serán introducidos por el usuario. Estos son los siguientes:
- [ ] accion: 
    - "TR": En caso de que se quiera realizar un entrenamiento 
    - "TS": En caso de que se quiera realizar un test (conjunto de datos de test dentro del Dataset de Clasificación)
    - "P": En caso de que se quiera realizar una predicción (conjunto de datos ajeno al Dataset de Clasificación)

En caso de que accion = TR:
- [ ] batch: 

Tamaño de batch a utilizar (se ha utilizado 64 ó 128)

- [ ] imgsize: 

Tamaño de la imagen cuadrado (en el proyecto, 256)
 
