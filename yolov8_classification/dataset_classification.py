import os
import random
import shutil

# Directorios de las carpetas
yolov8_folder = os.getcwd()
carpeta_original = os.path.join(yolov8_folder, "background22", "Dataset_global")
carpeta_entrenamiento = os.path.join(yolov8_folder, "background22", "Dataset_Clasificacion")

# Crear las carpetas train, val y test si no existen
for division in ['train', 'val', 'test']:
    division_carpeta = os.path.join(carpeta_entrenamiento, division)
    for clase in ['defects', 'non_defects']:
        clase_carpeta = os.path.join(division_carpeta, clase)
        if not os.path.exists(clase_carpeta):
            os.makedirs(clase_carpeta)

# Porcentaje de imágenes para cada conjunto
porcentaje_train = 0.7
porcentaje_val = 0.15
porcentaje_test = 0.15

# Recorrer las clases
for clase in ['defects', 'non_defects']:
    clase_original = os.path.join(carpeta_original, clase)
    clase_entrenamiento = os.path.join(carpeta_entrenamiento, 'train', clase)
    clase_val = os.path.join(carpeta_entrenamiento, 'val', clase)
    clase_test = os.path.join(carpeta_entrenamiento, 'test', clase)

    # Obtener la lista de nombres de imágenes en la carpeta original
    # nombres_imagenes = sorted(os.listdir(clase_original))
    nombres_imagenes = os.listdir(clase_original)

    # Calcular la cantidad de imágenes para cada conjunto
    total_imagenes = len(nombres_imagenes)
    num_train = int(total_imagenes * porcentaje_train)
    num_val = int(total_imagenes * porcentaje_val)
    num_test = total_imagenes - num_train - num_val

    # Seleccionar y mover las imágenes para cada conjunto
    # random.shuffle(nombres_imagenes)
    nombres_train = nombres_imagenes[:num_train]
    nombres_val = nombres_imagenes[num_train:num_train + num_val]
    nombres_test = nombres_imagenes[num_train + num_val:]

    for nombre in nombres_train:
        origen = os.path.join(clase_original, nombre)
        destino = os.path.join(clase_entrenamiento, nombre)
        shutil.copy2(origen, destino)

    for nombre in nombres_val:
        origen = os.path.join(clase_original, nombre)
        destino = os.path.join(clase_val, nombre)
        shutil.copy2(origen, destino)

    for nombre in nombres_test:
        origen = os.path.join(clase_original, nombre)
        destino = os.path.join(clase_test, nombre)
        shutil.copy2(origen, destino)

